#include "Deck.h"
#include <iostream>

#include <string>

#include <iterator>

#include <algorithm>

int main() {
	Deck<int> const deck { };
	std::cout << deck.empty();

	Deck<double> deck_init { 34, 24, 1, 31, 41, 4, 23 };
//	std::cout << deck_init.front();

	std::copy(deck_init.begin(), deck_init.end(), std::ostream_iterator<double> { std::cout, " " });

	std::string const s { "abcdef" };
	Deck<char> deck_iter { s.begin(), s.end() };
	std::cout << deck_iter.back();
}
