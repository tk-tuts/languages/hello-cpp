#ifndef INHERITANCE_H_
#define INHERITANCE_H_

#include <iostream>

struct instrument {
	instrument(std::ostream &out = std::cout) :
			out { out } {
		out << "Schlaginstrument gebaut\n";
	}

	~instrument() {
		out << "Schlaginstrument zerst�rt\n";
	}

	virtual void strike() const {
		out << "klick\n";
	}

	virtual void makeSound() const {
		out << "still\n";
	}

	std::ostream& out;
};

struct membranophone: instrument {
	using instrument::instrument;

	membranophone() {
		out << "Trommel gespannt\n";
	}

	~membranophone() {
		out << "Fell gerissen\n";
	}

	virtual void strike() {
		out << "boom\n";
	}

	void makeSound() const {
		out << "bamm\n";
	}

	virtual void tap() {
		out << "tack\n";
	}
};

struct bongo: membranophone {
	bongo(std::ostream &out) :
			membranophone { out } {
		out << "Bongo fertig\n";
	}

	~bongo() {
		out << "Bongo krack\n";
	}

	virtual void strike() {
		out << "tam-tam\n";
	}

	void makeSound() const {
		out << "bam-bam\n";
	}

	void tap() {
		out << "tap-tap\n";
	}
};

struct idiophone: instrument {
	using instrument::instrument;

	~idiophone() {
		out << "K�rperton tot\n";
	}

	void strike() const {
		out << "klack\n";
	}

	virtual void makeSound() const {
		out << "sch\n";
	}

	virtual void sustain() const = 0;
};

struct tubular_bells: idiophone {
	using idiophone::idiophone;

	tubular_bells() {
		out << "Branson kauft f�r Mike\n";
	}

	~tubular_bells() {
		out << "Oldfield fertig\n";
	}

	void strike() const {
		out << "boing\n";
	}

	void makeSound() const {
		out << "klong\n";
	}

	void sustain() const {
		out << "ngngngng\n";
	}
};

inline void runInheritance() {
	std::cout << "Inheritance is running \n\n";

	using std::cout;

	cout << "a ------\n";
	bongo b { std::cout }; // schlaginstrument gebaut, Bongo fertig
	idiophone const &tb = tubular_bells{}; // schlaginstrument gebaut, Branson kauft f�r Mike
	instrument const inst {b};
	membranophone &drum = b;

	cout << "b -----\n";
	inst.makeSound(); // still
	drum.makeSound(); // bam-bam
	b.makeSound(); // bam-bam
	tb.makeSound(); // klong
	tb.sustain(); // ngngngn

	cout << "c -----\n";
	inst.strike(); // klick
	drum.strike(); // tam-tam
	b.strike(); // tam-tam
	tb.strike(); // boing
	tb.sustain(); // ngngngn

	cout << "d -----\n";
	drum.tap(); // tap-tap
	b.tap(); // tap-tap

	cout << "end ----\n";
}
// inst: schlaginstrument zerst�rt
// tb ref --> but its original: Oldfield fertig, K�rperton tot, schlaginstrument zerst�rt
// b : bongo krakt, Fell gerissen, Schlaginstrument zerst�rt

#endif /* INHERITANCE_H_ */
