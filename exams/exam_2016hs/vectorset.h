#ifndef VECTORSET_H_
#define VECTORSET_H_

#include <vector>

#include <functional>

#include <iterator>

#include <algorithm>

#include <set>

template<typename T, typename COMPARE = std::less<T>>
struct vectorset : public std::vector<T>{
	using Base = std::vector<T>;
	using Base::vector;
	using size_type = typename Base::size_type;
	using iterator_type = typename Base::iterator;

	auto find(T value) const {
		return std::find(this->begin(), this->end(), value);
	}

	int count(T const value) const {
		return std::count(this->begin(), this->end(), value);
	}

	std::multiset<T, COMPARE> asMultiset() const{
		return std::multiset<T, COMPARE>{this->begin(), this->end()};
	}

	operator std::multiset<T, COMPARE>() const{
		return this->asMultiset();
	}

// todo find, count, compare functor
};



#endif /* VECTORSET_H_ */
