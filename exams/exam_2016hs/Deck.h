#ifndef DECK_H_
#define DECK_H_

#include <deque>
#include<initializer_list>
#include <stdexcept>

#include <algorithm>

template<typename T>
class Deck {
	using container = std::deque<T>;
	using size_type = typename container::size_type;
	using const_iterator = typename container::const_iterator;
	container c;
public:
	Deck() = default;

	Deck(std::initializer_list<T> list) :
			c { list } {
				shuffle();
	}

	template<typename ITER>
	Deck(ITER begin, ITER end) :
			c { begin, end } {
				shuffle();
	}

	bool empty() const {
		return c.empty();
	}
	size_type size() const {
		return c.size();
	}
	const T& front() const {
		return c.front();
	}
	const T& back() const {
		return c.back();
	}
	void push_back(T value) {
		c.push_back(value);
	}
	void pop_front() {
		if (c.empty()) {
			throw std::logic_error { "no items found" };
		}
		c.pop_front();
	}

	const_iterator begin() const{
		return c.begin();
	}

	const_iterator end() const{
		return c.end();
	}
private:
	void shuffle(){
		std::random_shuffle(c.begin(), c.end());
	}
};

#endif /* DECK_H_ */
