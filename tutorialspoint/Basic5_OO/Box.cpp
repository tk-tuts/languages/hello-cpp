//
// Created by Keerthikan on 11-Sep-17.
//
#include <iostream>
#include "Box.h"

Box::Box(double b){
    std::cout << "Object is being created" << std::endl;
    breadth = b;
}

Box::Box(double b, double h): breadth(b), height(h){
    std::cout << "Object is being created" << std::endl;
}

Box::~Box(void){
    std::cout << "Object is being deleted" << std::endl;
}

void Box::setLength(double len) {
    length = len;
}

double Box::getLength() {
    return length;
}

void handleBoxClass(std::ostream &out){
    out << "C++ Classes and Objects" << std::endl;

    Box box1{4.0};
    Box box2(13.0, 10.0);

    box1.height = 5.0;
    box1.setLength(13);

    box2.setLength(14);

    out << "Volume of Box1:" << box1.getVolume() << std::endl;
    out << "Volume of Box2:" << box2.getVolume() << std::endl;
}