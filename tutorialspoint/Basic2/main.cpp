#include "header.h"
#include <iostream>

int max(int, int);
void swapByPointers(int *x, int *y);
void swapByRef(int &x, int &y);

int main() {
    int x = 13;
    int y = 14;
    std::cout << "Which is greater? " << max(x, y) << std::endl;

    swapByPointers(&x, &y);
    std::cout << "x:" << x << "y:" << y << std::endl;

    swapByRef(x, y);
    std::cout << "x:" << x << "y:" << y << std::endl;

    sayBye(std::cout);

    return 0;
}

int max(int num1, int num2){
    return num1 > num2 ? num1 : num2;
}

void swapByPointers(int *num1, int *num2){
    int temp;
    temp = *num1;
    *num1 = *num2;
    *num2 = temp;
}

void swapByRef(int &num1, int &num2){
    int temp = num1;
    num1 = num2;
    num2 = temp;
}

void sayBye(std::ostream &out){
    out << "Good bye world" << std::endl;
}