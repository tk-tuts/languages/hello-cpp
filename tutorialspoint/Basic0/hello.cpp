#include <iostream>
using namespace std;

int main(){
    cout << "Hello TutorialPoint\n";

    // size of the data types
    cout << "Size of char :" << sizeof(char) << endl;

    typedef int age;
    age a1 = 2;
    cout << "type age :" << a1 << endl;

    enum color {red = 1, green, blue};
    color c = red;
    cout << "My fav color is " << c << endl;
    cout << "My second fav color is " << blue << endl;
    return 0;
}