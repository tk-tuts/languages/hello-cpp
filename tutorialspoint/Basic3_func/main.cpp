#include <iostream>
#include <cmath>
#include <ctime>

void proceed_array();

int main() {

    int a = 10;
    int b{11};

    double const pi = 3.14;
    double constexpr pi2{3.14};

    std::cout << "Hello, Functions" << std::endl;
    std::cout << a << std::endl;
    std::cout << b << std::endl;

    std::cout << pi2 << std::endl;

    std::cout << "expression:" << (5 + 10 * 3 - 7 / 2) << std::endl;
    auto x = 3 / 2;
    std::cout << "x:" << x << std::endl;

    double y = 45 / 8;
    std::cout << "y:" << y << std::endl;

    std::cout << "cos(0):" << cos(0) << std::endl;

    // random number
    srand((unsigned) time (NULL));
    std::cout << "Random number:" << rand() << std::endl;

    proceed_array();
    return 0;
}

void printIntArray(double array[], int size){
    for(int i=0; i < size; i++){
        std::cout << "i:" << i << " - " << array[i] << std::endl;
    }
}

void printIntPointer(int *array, int size){
    for(int i=0; i < size; i++){
        std::cout << "i:" << i << " - " << *(array + i) << std::endl;
    }
}

void proceed_array(){
    double numbers[3];
    numbers[0] = 13;
    numbers[1] = 14;

    int numbers2[2] = {10, 11};

    std::cout << "Array" << std::endl;
    std::cout << "size of numbers0: bytes" << sizeof(numbers) << std::endl;
    std::cout << numbers[0] << std::endl;
    std::cout << numbers2[0] << std::endl;
    std::cout << *(numbers2 + 1)<< std::endl;
    printIntArray(numbers, 3);
}
