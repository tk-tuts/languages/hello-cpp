#include <iostream>

class Shape {
public:
    Shape(int a = 0, int b = 0) {
        width = a;
        height = b;
    }

    void setWidth(int width) {
        this->width = width;
    }

    void setHeight(int h) {
        height = h;
    }

    int getArea(){
        std::cout << "Parent class" << std::endl;
        return 0;
    }

    virtual int getArea2() = 0;

    void printName(){
        std::cout <<"Shape" << std::endl;
    }

protected:
    int width;
    int height;
};

class Rectangle : public Shape {
public:
    Rectangle(int a = 0, int b = 0) : Shape(a, b) {}

    int getArea() {
        return this->height * this->width;
    }

    int getArea2(){
        return this->getArea();
    }

    virtual void printName(){
        std::cout <<"Rectangle" << std::endl;
    }
};

int main() {
    std::cout << "Inheritance" << std::endl;

    Rectangle rect;
    rect.setHeight(5);
    rect.setWidth(7);

    Shape *shape = &rect;

    rect.printName();
    std::cout << "Total Area of Rectangle: " << rect.getArea() << std::endl;
    std::cout << "Total Area of Rectangle: " << rect.getArea2() << std::endl;
    shape->printName();
    std::cout << "Total Area of Shape: " << shape->getArea() << std::endl;
    std::cout << "Total Area of Shape virtual: " << shape->getArea2() << std::endl;

    return 0;
}