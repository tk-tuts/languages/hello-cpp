#include <vector>

#include <iostream>

#include <algorithm>

#include <numeric>

#include <iterator>

#include <string>

void println(int x, std::ostream& out, std::string prefix = "println "){
	out << prefix << x << "\n";
}

template<typename T>
void printAll(std::vector<T> v, std::ostream& out){
	std::for_each(v.begin(), v.end(), [&out](auto x) -> void {
		println(x, out);
	});
}

int main(){
	std::vector<int> numbers{};

	numbers.push_back(3);
	numbers.push_back(4);

	numbers.insert(numbers.begin()+2, 5);
	numbers.insert(numbers.begin(), 1);

	for(auto it = numbers.begin(); it != numbers.end(); ++it){
			std::cout << (*it)++ << "\n";
	}

	std::cout << "after\n";

	// count occurrences
	size_t odds = std::count(numbers.begin(), numbers.end(), 2);
	println(odds, std::cout, "Occurrences");

	// total
	std::cout << "Total: " << std::accumulate(numbers.begin(), numbers.end(), 0) << "\n";

	std::string keerthikan{"Keerthikan"};
	std::cout << "Distance " << std::distance(keerthikan.begin(), keerthikan.end()) << "\n";

	printAll(numbers, std::cout);
	std::cout << "more algorithms\n";

	std::vector<int> numbers2(3);

	int init{5};
	std::generate_n(std::back_inserter(numbers2), 5, [&init]{ return init *= 4;});
	std::copy(numbers2.begin(), numbers2.end(), std::back_inserter(numbers));
	printAll(numbers, std::cout);

	std::cout << "stream algorithms\n";
	std::copy(numbers.begin(), numbers.end(), std::ostream_iterator<int>{std::cout, ", "});

	std::vector<int> numbers3{};
	using numInput = std::istream_iterator<int>;
	numInput eof{};
	numInput input{std::cin};
	std::copy(input, eof, std::back_inserter(numbers3));
	printAll(numbers3, std::cout);

	std::vector<int> numbers4(4);
	printAll(numbers4, std::cout);
}
