#include <iostream>

enum class day_of_week {
	Mon, Tue, Wed, Thu, Fri, Sat, Sun
};
//Enumerators are visible here

enum DayOfWeek {
	Mon, Tue, Wed, Thu, Fri, Sat, Sun
};

int main(){
	std::cout<< "Enum is on the way\n";

	day_of_week d1{day_of_week::Mon};
	DayOfWeek d2{Tue};

	// class enum should casted explicitly
	int intd1 = static_cast<int>(d1);
	int intd2 = d2;

	std::cout << intd1;
	std::cout << intd2;
}
