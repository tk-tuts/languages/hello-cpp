/*
 * MyVector.h
 *
 *  Created on: Jan 23, 2018
 *      Author: Keerthikan
 */

#ifndef MYVECTOR_H_
#define MYVECTOR_H_

#include <vector>

template<typename T>
class MyVector: public std::vector<T>{
public:
	T const & operator[](int index) const;
};

#endif /* MYVECTOR_H_ */
