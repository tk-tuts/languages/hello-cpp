#include <iostream>

int doubleNumber(int num){
	return num * 2;
}

int main(){
	int (&doubleFunc)(int) = doubleNumber;
	auto dFunc = doubleNumber;
	std::cout << "2: " << doubleFunc(2) << "\n";
	std::cout << "3: " << dFunc(3) << "\n";

	auto const trippleFunc = [](int n) -> int {
		return n * 3;
	};
	std::cout << "2: " << trippleFunc(2) << "\n";

	int x = 5;
	auto func2 = [&]{
		x++;
	};
	func2();
	std::cout << "x: " << x << "\n";
}
