#include "Date.h"

#include <iostream>

int main(){
	Date d{1991, 11, 14};
	Date d2{1994, 13, 04};
	Date d3{1991, 11, 13};
	std::cout << d.getYear() << "\n";
	std::cout << (d < d2) << "\n";
	std::cout << (d < d3) << "\n";
}



