#include <iostream>

#include <vector>

#include <algorithm>

#include <functional>

#include <iterator>

struct Averager{
	void operator()(double d){
		accumulator += d;
	}
	double sum() const {
		return accumulator;
	}
private:
	double accumulator{};
};

void applyAndPrint(std::ostream& out, std::function<bool(int)> aPredicate){
	if(aPredicate){
		out << "pred(13) is" << aPredicate(13) << "\n";
	}else{
		out << "empty function holder\n";
	}
}

int main(){
	Averager f1{};
	std::vector<double> v {1,2,3,4,5};
	auto const res = std::for_each(v.begin(), v.end(), f1);
	std::cout << "Value: " << res.sum() << "\n";

	// f1.sum() will not work
	std::cout << "Value2: " << f1.sum() << "\n";

	std::vector<double> v2 {10, 20, 30, 40 ,50};
	std::vector<double> v3{};
	transform(v.begin(), v.end(), v2.begin(), std::back_inserter(v3), std::multiplies<>{});

	std::copy(v3.begin(), v3.end(), std::ostream_iterator<double>(std::cout, " "));

	std::function<bool(int)> aPredicate{};
	applyAndPrint(std::cout, aPredicate);

	aPredicate = [](int i){ return i%2;};
	applyAndPrint(std::cout, aPredicate);
}
