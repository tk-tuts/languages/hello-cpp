#include <deque>
#include <algorithm>

#include <vector>

#include <iostream>

#include <iterator>

#include <set>

#include <map>

int main() {
	std::deque<int> q { 1, 2, 3, 4 };
	std::vector<int> v { 1, 2, 3, 4 };
	std::set<int> s { 1, 2, 3, 4, 5, 5 };
	std::map<char, short> m { { 'a', 1 }, { 'b', 2 } };

	auto output = std::ostream_iterator<int>(std::cout, ", ");

	std::cout << "Vector: ";
	std::copy(v.begin(), v.end(), output);

	q.push_front(0);

	std::cout << "\n";
	std::cout << "Queue: ";
	std::copy(q.begin(), q.end(), output);

	std::cout << "\n";
	std::cout << "Set: " << s.size();

	std::cout << "\n";
	std::cout << "Map:\n";

	m['c'] = 3;
	for(auto const &p : m){
		std::cout << p.first << " = " << p.second << '\n';
	}

	std::multiset<char> letters{'a', 'a', 'c', 'c', 'c'};
	// no output
	copy(letters.lower_bound('b'), letters.upper_bound('b'), std::ostream_iterator<char>{std::cout, ", "});

	copy(letters.lower_bound('c'), letters.upper_bound('c'), std::ostream_iterator<char>{std::cout, ", "});


}
